# franq-project

# How to develop
1. clone this repo
2. Create a virtualenv
3. Activate the virtualenv
4. Install dependencies
5. Update setting to postgress
6. Run the migrations
7. Run development server
8. Open localhost:8000

`git clone https://github.com/saitoHugo/franq-project.git`

`cd franq`

`python -mvenv .venv`

`source .venv/bin/activate`

`pip install -r requirements.txt`

`python manage.py makemigrations`

`python manage.py migrate`

`python manage.py runserver`

# Exercícios do Desafio: 

**1.Crie uma aplicação django que guarde os dados cadastrais de uma pessoa; não precisa realizar um cadastro completo, apenas nome, telefone e e-mail já é suficiente. Para facilitar o entendimento, vamos chamar essa aplicação de Pessoa.**
- Realizado (app pesosoa)

**2.Crie uma segunda aplicação django para gerenciar garagens de veículos e seus respectivos veículos. Vamos chamar essa aplicação de Garagem.**
- Realizado (app pesosoa)
-
**3.Crie dois tipos de pessoas, uma é o consumidor, e outra é o administrador do sistema e defina seus respectivos escopos de acessos para ambas aplicações.**
- Não Realizado - Apenas autenticação foi feita, autorização não. 

**4.Construa uma API entre as duas aplicações, onde para cada pessoa cadastrada seja criada uma garagem para ela na segunda aplicação (Garagem).**
- Realizado - Não foi necessário a ciração de uma API externa, isso é feito através de um signal do django onde para cada perfil criado no app Pessoa um perfil no app garagem também é criado (GeragemProfile)

**5.Na aplicação Garagem forneça ao usuário a lista de veículos para que ele possa vincular a sua garagem e em seguida faça um endpoint para exibir todos os veículos que esse respectivo usuário escolheu para deixar em sua garagem.**
- Realizado - na aplicação garagem temos o endpoint para mostrar todas as informações do usuário (incluindo sua lista de veículos e informação sobre sua garagem). Esse endpoint fornece a opção de editar sua vaga selecionando entre as opções de seus veículos. 

**6.Para cada veículo crie um método que retorne a sua cor e seu ano de fabricação, porém, se o veículo for uma moto, deve retornar o modelo no lugar da cor junto com o ano de fabricação.**
- Realizado: (em fase de teste)

**7.Apenas os dados de e-mail e telefônico podem ser persistidos na aplicação garagem sendo assim, todos os demais dados devem permanecer na aplicação Pessoa.**
- Não Realizado. Não compreendi muito bem e acabei utilizando um unico banco de dados (pessoa-db) do postgress.

**8.Por fim crie uma API para uma aplicação terceira consultar todos os clientes cadastrados, todas as garagens ativas, e quais clientes possuem veículos vinculados a suas garagens e quais não possuem.**
- Não Realziado. 

**9.Faça uso das boas práticas de desenvolvimento e segurança de API’s e bem como para login dos usuários.**

**10.Faça uso do git para versionar o seu código e crie um repositório no https://bitbucket.org/ para realizer esse teste, quando concluído nos envie o acesso ao repositório para avaliarmos o resultado.**



# Dúvidas e Melhorias

- Definir autorização   pelo o pouco que eu vi poderia ser feita por meio da gestão dos grupos do djando criando o grupo administrador e consumidor (não apenas Users - django default como foi feito) e definir dentro da classe MEta de da model as permissões. Cmo a referência [Autenticação de usuário e permissões](https://developer.mozilla.org/pt-BR/docs/Learn/Server-side/Django/Authentication)

```
class BookInstance(models.Model):
    ...
    class Meta:
        ...
        permissions = (("can_mark_returned", "Set book as returned"),)
```

Também seria necesário alterar os decoradores das view para adcionar o reuqerimento de permissão:

```
from django.contrib.auth.decorators import permission_required

@permission_required('catalog.can_mark_returned')
@permission_required('catalog.can_edit')
def my_view(request):
    ...

```


# Referências

Segue abiaxo a lista de algumas das referências utilizadas para o desenvolvimento do projeto. 

- [Relationships in Django models](https://www.webforefront.com/django/setuprelationshipsdjangomodels.html)
- [django-phonenumber-field](https://github.com/stefanfoulis/django-phonenumber-field)
- [PostgreSQL no Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart-pt)
- [User Sign Up View in Django](https://dev.to/coderasha/create-advanced-user-sign-up-view-in-django-step-by-step-k9m)
- [Resetting Django Migrations](https://www.techiediaries.com/resetting-django-migrations/)

- [django.utils.functional](https://docs.djangoproject.com/en/3.2/ref/utils/#:~:text=The%20%40cached_property%20decorator%20caches%20the,cached%20result%20will%20be%20returned.)