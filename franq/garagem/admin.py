from django.contrib import admin

# Register your models here.
from .models import GarageProfile, Veiculo, Vaga
# Register your models here.


class BaseAdmin(admin.ModelAdmin):
    exclude = ["created", "modified"]

    class Meta:
        abstract = True

@admin.register(GarageProfile)
class GarageProfileAdmin(BaseAdmin):
    list_display = ("pessoa_profile", "bio")
    fields = ("pessoa_profile", "bio")

@admin.register(Veiculo)
class VeiculoAdmin(BaseAdmin):
    list_display = ("garage_profile", "tipo", "cor", "ano", "modelo")
    fields = ("garage_profile", "tipo", "cor", "ano", "modelo")

@admin.register(Vaga)
class VagaAdmin(BaseAdmin):
    list_display = ("garage_profile", "ocupada")
    fields = ("garage_profile", "ocupada")