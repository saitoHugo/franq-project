from django import forms
#from django.contrib.auth.models import User
from .models import Veiculo, Vaga
from django.contrib.auth.forms import UserCreationForm
from phonenumber_field.formfields import PhoneNumberField

class VeiculoForm(forms.ModelForm):
    #username = forms.CharField(max_length=30)
    #name = forms.CharField(help_text="Nome Completo")
    #email = forms.EmailField( help_text="Ex: hello@email.com")
    ##phone = forms.CharField( help_text="Ex: 11 99999 0000") #TODO: update to phoneforms
    #phone = PhoneNumberField(
    #    label="Telefone: 11 99900-1122",
    #)

    class Meta:
        model = Veiculo #User
        fields = ('tipo', 'cor', 'ano', 'modelo') #'garage_profile' )#('username', 'name', 'email', 'phone', 'password1', 'password2', )
    
    #def save(self, commit=True):
    #    user = super(RegistrationForm, self).save(commit=False)
    #    user.email = self.cleaned_data['email']
    #
    #    if commit:
    #        user.save()
    #
    #    return user

class VagaForm(forms.ModelForm):
    #username = forms.CharField(max_length=30)
    #name = forms.CharField(help_text="Nome Completo")
    #email = forms.EmailField( help_text="Ex: hello@email.com")
    ##phone = forms.CharField( help_text="Ex: 11 99999 0000") #TODO: update to phoneforms
    #phone = PhoneNumberField(
    #    label="Telefone: 11 99900-1122",
    #)
    #cars = forms.ModelMultipleChoiceField(queryset=)

    class Meta:
        model = Vaga #User
        fields = ('ocupada',) #'garage_profile' )#('username', 'name', 'email', 'phone', 'password1', 'password2', )