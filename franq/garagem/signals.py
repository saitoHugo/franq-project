#from django.contrib.auth.models import User
from pessoa.models import Profile
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
from .models import GarageProfile, Vaga

import logging
logger = logging.getLogger('pessoa')

@receiver(post_save, sender=Profile)
def create_garage_profile_signal(sender, instance, created, **kwargs):
    logger.info("update_garage_profile_signal")
    logger.info("Created - {}".format(created))
    logger.info("Instance - {}".format(type(instance)))
    #created = True
    #logger.info("Created FORCED True - ",created )
    if created: #Create new User and save
        logger.info("Create new GarageProfile")
        GarageProfile.objects.create(pessoa_profile=instance)
        logger.info("new GarageProfile created")
    #    #instance.profile.save()
    else: #save updated instance
        logger.info("NOT new GarageProfile")
        #profile = get_object_or_404(GarageProfile, pessoa_profile=instance)
        #profile.save()
    #else:
    #    profile = get_object_or_404(Profile, instance)
    #    profile.save()


@receiver(post_save, sender=GarageProfile)
def create_vaga_garage_profile_signal(sender, instance, created, **kwargs):
    logger.info("create_vaga_garage_profile_signal")
    logger.info("Created - {}".format(created))
    logger.info("Instance - {}".format(type(instance)))
    #created = True
    #logger.info("Created FORCED True - ",created )
    if created: #Create new Vaga
        logger.info("Create new Vaga")
        Vaga.objects.create(garage_profile=instance)
        logger.info("new Vaga created")
    #    #instance.profile.save()
    else: #save updated instance
        logger.info("NOT new Vaga")
        #vaga = get_object_or_404(GarageProfile, pessoa_profile=instance)
        #vaga.save()
    #else:
    #    profile = get_object_or_404(Profile, instance)
    #    profile.save()