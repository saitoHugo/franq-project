
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import login, authenticate
from .forms import VeiculoForm, VagaForm
from .models import GarageProfile, Veiculo, Vaga
from django.contrib.auth.decorators import login_required
from pessoa import models as pessoa_models

import logging
logger = logging.getLogger('garagem')

# Create your views here.
@login_required
def cadastrar_veiculo(request):
    #only render home template
    #return render (request=request, template_name='cadastrar_veiculo.html')


    #def signup_page(request):
    logger.info("cadastrar_veiculo view")
    
    if request.method == 'POST':
        logger.info("POST REQUEST")
        
        #TODO: completar garage_profile in form
        logger.info("request.user - {}".format(request.user))
        logger.info("request.user - {}".format(type(request.user)))
        pessoa_profile = get_object_or_404(pessoa_models.Profile, user=request.user)
        logger.info("pessoa_profile retrieved - ", pessoa_profile)
        logger.info("pessoa_profile retrieved type - ", type(pessoa_profile))
        garage_profile = get_object_or_404(GarageProfile, pessoa_profile=pessoa_profile)
        logger.info("Garag Profile retrieved - ", garage_profile)
        logger.info("Garag Profile retrieved type - ", type(garage_profile))

        form = VeiculoForm(request.POST)#, garage_profile=garage_profile)
        form.instance.garage_profile = garage_profile
        if form.is_valid():
            logger.info("Valid Form")
            logger.info("Form type - {}".format(type(form)))
            logger.info(form.instance)
            logger.info("Instance type - ", type(form.instance))
            veiculo = form.save()
            #veiculo = Veiculo.objects.create()
            logger.info("veiculo form.save() - {}".format(veiculo))
            logger.info("veiculo form.save() type - {}".format(type(veiculo)))

            return redirect('home')
        else:
            logger.info("Not a Valid Form")
            return render(request=request, template_name='cadastrar_veiculo.html', context={'form': form})
    else:
        logger.info("NOT a POST request")
        form = VeiculoForm()    
        return render(request=request, template_name='cadastrar_veiculo.html', context={'form': form})
    

@login_required
def garage_profile_detail(request):
    #def signup_page(request):
    logger.info("garage_profile_detail view")
    
    if request.method == 'GET':
        logger.info("GET REQUEST")
        
        #TODO: completar garage_profile in form
        logger.info("request.user - {}".format(request.user))
        logger.info("request.user - {}".format(type(request.user)))
        pessoa_profile = get_object_or_404(pessoa_models.Profile, user=request.user)
        logger.info("pessoa_profile retrieved - {}".format(pessoa_profile))
        logger.info("pessoa_profile retrieved type - {}".format(type(pessoa_profile)))
        garage_profile = get_object_or_404(GarageProfile, pessoa_profile=pessoa_profile)
        logger.info("Garag Profile retrieved - {}".format(garage_profile))
        logger.info("Garag Profile retrieved type - {}".format(type(garage_profile)))
        vaga = get_object_or_404(Vaga, garage_profile=garage_profile)
        #form = VeiculoForm(request.POST)#, garage_profile=garage_profile)
        #form.instance.garage_profile = garage_profile

        #get all veiculos
        #veiculos = Veiculo.objects.get
        #veiculos = garage_profile.veiculo_set()
        veiculos = Veiculo.objects.filter(garage_profile=garage_profile)
        logger.info("veiculos - {}".format(veiculos))
        context = {
            "pessoa_profile":pessoa_profile,
            "garage_profile":garage_profile,
            "vaga":vaga,
            "veiculos_list":veiculos,
        }
        return render(request, 'garage_profile_detail.html', context=context)


# Create your views here.
@login_required
def atualizar_vaga(request):
    #only render home template
    #return render (request=request, template_name='cadastrar_veiculo.html')


    #def signup_page(request):
    logger.info("atualizar_vaga view")
    #TODO: completar garage_profile in form
    logger.info("request.user - {}".format(request.user))
    logger.info("request.user - {}".format(type(request.user)))
    pessoa_profile = get_object_or_404(pessoa_models.Profile, user=request.user)
    logger.info("pessoa_profile retrieved - {}".format(pessoa_profile))
    logger.info("pessoa_profile retrieved type - {}".format(type(pessoa_profile)))
    garage_profile = get_object_or_404(GarageProfile, pessoa_profile=pessoa_profile)
    logger.info("Garag Profile retrieved - {}".format(garage_profile))
    logger.info("Garag Profile retrieved type - {}".format(type(garage_profile)))
    vaga = get_object_or_404(Vaga, garage_profile=garage_profile)
    if request.method == 'POST':
        logger.info("POST REQUEST")
        form = VagaForm(request.POST)
        print("form - ", form.__dict__)
        print("form type - ", type(form))
        #vaga = form.save()
        #print("vaga - ", vaga)
        #print("vaga type- ", type(vaga))
        #print("vaga.instance  - ", vaga.instance)
        #logging.info("form.instance.garage_profile - {}".format(form.instance.garage_profile))
        #form.instance.garage_profile = garage_profile
        if form.is_valid():
            logger.info("Valid Form")
            logger.info("Form type - {}".format(type(form)))
            logger.info(form.cleaned_data)
            logger.info("Form ocupada - {}".format(form.cleaned_data['ocupada']))
            logger.info("Form ocupada type - {}".format(form.cleaned_data['ocupada']))
            
            logger.info("Vaga - {}".format(vaga))
            logger.info("Vaga type - {}".format(type(vaga)))
            logger.info("Vaga.ocupada - {}".format(vaga.ocupada))
            logger.info("Vaga.ocupada type - {}".format(type(vaga.ocupada)))
            vaga.ocupada = form.cleaned_data['ocupada']
            vaga.save()
            #vaga = form.save()
            #veiculo = Veiculo.objects.create()
            logger.info("vaga form.save() - {}".format(vaga.__dict__))
            logger.info("vaga form.save() type - {}".format(type(vaga)))            
            return redirect('garage_profile_detail')
        else:
            logger.info("Not a Valid Form")
            return render(request=request, template_name='atualizar_vaga.html.html', context={'form': form})
    else:
        logger.info("GET request")
        #TODO: preencher VagaForm com dados (Vaga id e ocupada/veiculo) do usuário 
        vaga = get_object_or_404(Vaga, garage_profile=garage_profile)
        form = VagaForm(instance=vaga)    
        return render(request=request, template_name='atualizar_vaga.html', context={'form': form})