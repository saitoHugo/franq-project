from .views import cadastrar_veiculo, atualizar_vaga, garage_profile_detail
from django.urls import path, include
#from django.contrib import auth

#from .views import SignUpView


urlpatterns = [
    path('veiculo-new/', cadastrar_veiculo, name="cadastrar_veiculo"),
    path('atualizar-vaga/', atualizar_vaga, name="atualizar_vaga"),
    path('profile-detail/', garage_profile_detail, name="garage_profile_detail"),
    
    #path('auth/signup/', signup_page, name="signup"),
    #path('auth/', include('django.contrib.auth.urls')), #add auth urls
    #path('signup/', SignUpView.as_view(), name='signup'),
    
]