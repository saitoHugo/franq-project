# Create your models here.

from django.db import models
from django.contrib.auth.models import User
from django.db.models.base import Model
from django.db.models.signals import post_save
from django.dispatch import receiver
#from phonenumber_field.modelfields import PhoneNumberField
from pessoa import models as pessoa_models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.functional import cached_property


"""
TODO: Model GarageProfile
        - PessoaProfile
        - Garagem (carroList = [ Veiculo, Veiculo, Veiculo ])
        - Vaga
"""
class GarageProfile(models.Model):
    pessoa_profile = models.OneToOneField(pessoa_models.Profile, on_delete=models.CASCADE, primary_key=True) #models.ForeignKey(pessoa_models.Profile, on_delete=models.CASCADE)
    #rg = models.IntegerField(blank=True)
    #cpf = models.IntegerField(blank=True)
    bio = models.CharField(max_length=500, blank=True)
   
    def __str__(self):
        return f'GarageProfile: {self.pessoa_profile} '

    

"""
TODO: Model Veículo
        - tipo: carro ou moto
        - cor
        - ano
        - modelo

        Métodos
            - veiculo_info
                #check veiculo tipo
                    #moto
                        #retorna  modelo e ano
                    #carro
                        #retorna cor e ano

"""

class Veiculo(models.Model):
    garage_profile = models.ForeignKey(GarageProfile, on_delete=models.CASCADE)
    TIPOS = (
        ('moto', ('Moto')),
       ('carro', ('Carro')),
    )
    tipo = models.CharField(
       max_length=32,
       choices=TIPOS,
       default='carro',
   )
    cor = models.CharField(max_length=100, blank=True)
    ano = models.IntegerField(validators=[MinValueValidator(1900), MaxValueValidator(2022)], blank=True)
    modelo = models.CharField(max_length=100, blank=True)
    
    def __str__(self):
        return f'Veiculo: {self.tipo}, {self.modelo}, {self.cor} '
    
    @cached_property
    def info(self):
        if self.tipo == 'carro': #self.TIPOS['carro']:
            return self.ano, self.cor
        
        else:
            return self.ano, self.modelo


"""
TODO: Model Vaga
        - identificação (número)
        - ocupacao (Garagem ID)
"""

class Vaga(models.Model):
    garage_profile = models.OneToOneField(GarageProfile, on_delete=models.CASCADE, primary_key=True) #models.ForeignKey(GarageProfile, on_delete=models.CASCADE)
    #identidade = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(500)])
    ocupada = models.OneToOneField(Veiculo, on_delete=models.DO_NOTHING, primary_key=False, null=True, blank=True)
    #ocupada = models.BooleanField(default=False)#models.ForeignKey(Veiculo, on_delete=models.DO_NOTHING)
    
    def __str__(self):
        return f'Vaga: {self.ocupada}'