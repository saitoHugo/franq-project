from .models import Profile
from django.shortcuts import get_object_or_404, render
#from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm
from django.urls import reverse_lazy
#from django.views import generic
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect

import logging
logger = logging.getLogger('pessoa')

#class SignUpView(generic.CreateView):
#    form_class = UserCreationForm
#    success_url = reverse_lazy('login')
#    template_name = 'registration/signup.html'
def home_page(request):
    #only render home template
    return render (request=request, template_name='home.html')

def signup_page(request):
    logger.info("signup_pagev view")
    
    if request.method == 'POST':
        logger.info("POST REQUEST")
        #get user form
        #form = UserCreationForm(request.POST)
        form = SignUpForm(request.POST)
        if form.is_valid():
            logger.info("Valid Form")
            user = form.save()
            logger.info("User form.save() - {}".format(user))
            logger.info("User form.save() type - {}".format(type(user)))
            user.refresh_from_db()
            logger.info("DB Refresh") # load the profile instance created by the signal

            profile = get_object_or_404(Profile, user=user)
            logger.info("Profile created - {}".format(profile))
            
            logger.info("populating profile fields")
            profile.name = form.cleaned_data.get('name')
            profile.email= form.cleaned_data.get('email')
            profile.phone= form.cleaned_data.get('phone')
            logger.info("profile fields populated")
            #logger.info("saving profile")
            profile.save()  
            #logger.info("profile saved")
            logger.info("NEW profile fields")
            logger.info("profile.name - {}".format(profile.name))
            logger.info("profile.email - {}".format(profile.email))
            logger.info("profile.phone - {}".format(profile.phone))
                  
            #get username form data
            username = form.cleaned_data.get('username')
            #get password form data
            password = form.cleaned_data.get('password1')
            #get autenticated user
            user = authenticate(username=username, password=password)

            #save user ID in session 
            login(request, user)

            #redirect logged user to home
            return redirect('home')
        else:
            logger.info("Not a Valid Form")
            return render(request=request, template_name='signup.html', context={'form': form})
    else:
        logger.info("NOT a POST request")
        form = SignUpForm()    
        return render(request=request, template_name='signup.html', context={'form': form})