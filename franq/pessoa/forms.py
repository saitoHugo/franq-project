from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from phonenumber_field.formfields import PhoneNumberField

class SignUpForm(UserCreationForm):
    #username = forms.CharField(max_length=30)
    name = forms.CharField(help_text="Nome Completo")
    email = forms.EmailField( help_text="Ex: hello@email.com")
    #phone = forms.CharField( help_text="Ex: 11 99999 0000") #TODO: update to phoneforms
    phone = PhoneNumberField(
        label="Telefone: 11 99900-1122",
    )

    class Meta:
        model = User
        fields = ('username', 'name', 'email', 'phone', 'password1', 'password2', )