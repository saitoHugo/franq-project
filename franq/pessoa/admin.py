from django.contrib import admin
from .models import Profile
# Register your models here.


class BaseAdmin(admin.ModelAdmin):
    exclude = ["created", "modified"]

    class Meta:
        abstract = True

@admin.register(Profile)
class ProfileAdmin(BaseAdmin):
    list_display = ("user", "name", "phone", 'email')
    fields = ("user", "name", "phone", "email")
