from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
from .models import Profile

import logging
logger = logging.getLogger('pessoa')

@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    logger.info("update_profile_signal")
    logger.info("Created - {}".format(created))
    logger.info("Instance - {}".format(type(instance)))
    #created = True
    #logger.info("Created FORCED True - ",created )
    if created: #Create new User and save
        logger.info("Create new Profile")
        Profile.objects.create(user=instance)
        logger.info("new profile created")
    #    #instance.profile.save()
    else: #save updated instance
        logger.info("NOT new Profile")
        #profile = get_object_or_404(Profile, user=instance)
        #profile.save()
    #else:
    #    profile = get_object_or_404(Profile, instance)
    #    profile.save()