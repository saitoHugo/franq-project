from .views import home_page, signup_page
from django.urls import path, include
from django.contrib import auth

#from .views import SignUpView


urlpatterns = [
    path('', home_page, name="home"),
    path('auth/signup/', signup_page, name="signup"),
    path('auth/', include('django.contrib.auth.urls')), #add auth urls
    #path('signup/', SignUpView.as_view(), name='signup'),
    
]